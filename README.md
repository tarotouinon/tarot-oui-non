C'est une étude du [tarot oui non](https://www.tarotdesdieux.com/2017/02/tarot-oui-non.html) à faire. L'habitude aidant, on y arrivera sans fatigue. Et maintenant, choisissez voire méthode, étudiez vos combinaisons, et, quand vous serez sûr de vous, passez au chapitre suivant qui vous apprendra comment on se sert des cartes pour leur arracher le secret de l'avenir.

Les cartes du tarot oui non mêlées, on les coupe, ou bien on les fait couper par le consultant, en deux tas a peu près égaux. Le consultant choisit un de ces deux tas. La première carte est retirée et mise en réserve. Le reste du paquet est retourné et on en lit l'explication, d'après nos données précédentes.

Supposons que le paquet de tarot oui non se compose de quinze cartes, tirées dans cet ordre : As de cœur, neuf de trèfle, roi de cœur, dix de carreau, neuf de cœur, huit de cœur, huit de carreau, sept de trèfle, sept de carreau, sept de cœur, as de carreau, dame de pique, valet de carreau, as de trèfle. La dernière, le huit de trèfle, est mise en réserve.
Lisez couramment, sans lier les cartes une aune, et cherchez un sens dans leur accouplement partiel:
« Dans ma maison, un héritage qu'un homme blond qui revient de voyage m'apporte, sûr de réussir dans son amour. Il y aura bien des moqueries pour ce petit héritage (sept de trèfle et sept de carreau), mais une lettre d'une veuve apportée par le facteur ou un militaire confirmera la bonne nouvelle. »

Reprenez les quinze cartes du tarot oui non, mêlez-les et distribuez-les en trois tas, en mettant encore une carte de réserve. Le consultant choisit un de ces tas, dans lequel se trouvent, par exemple :
Le huit et le dix de carreau, le roi de cœur, le sept de trèfle et l'as de carreau.
« Vos démarches amoureuses aboutiront à un voyage pour lequel un homme blond vous enverra de l'argent par lettre. »